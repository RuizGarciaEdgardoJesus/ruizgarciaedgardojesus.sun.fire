<?php
$app->delete('/indicador/:id', function($id) use($app) {
  /*Validaciones*/
  if (id_invalido($id)) {
    $app->halt(400, "Error: El id '$id' no cumple con el formato correcto.");
  } 
	$id_indicador=$id;

  $consulta="delete from indicador where id_indicador='".$id_indicador."'"; 
  /* EliminaciÃ³n en la bd*/ 
  $qry = $app->db->prepare($consulta);


  if ($qry->execute() === false) {
    /* AlgÃºn error se presentÃ³ con la bd*/ 
    $app->halt(500, "Error: No se ha podido borrar el indicador con id '$id'.");
  }
	if ($qry->rowCount() ==0) {
    /* No se borrÃ³ ningÃºn libro en la bd */
    $app->halt(404, "Error: El indicador con id '$id' no existe.");
  }



  /* En caso de exito en el borrado del libro en la bd, se le indica al cliente*/ 
  $app->halt(200, "Exito: El indicador con id '$id' ha sido borrado");
});
